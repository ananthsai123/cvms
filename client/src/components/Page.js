import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Register from './Register';
import Show from './Show'; 
import Login from './Login';

function Page() {
    return (
    <BrowserRouter>
      <Routes>
        <Route path='/reg' element={<Register />} />
        <Route path='/sho' element={<Show />} />
        <Route path='/log' element={<Login />} />
      </Routes>
    </BrowserRouter>
    );
  }

  export default Page